package com.youyou.storemanage.constant;

/**
 * 异常常量 这里放置不需要错误码的常量
 * @Description: 异常常量
 * @see ErrorCodeConst 同步这
 */
public class ExceptionConst {
    /**
     * 密码不应该为空
     */
    public static final String PASSWORD_NOT_EMPTY = "密码不应该为空!";
    /**
     * 账号或者密码错误
     */
    public static final String PASSWORD_OR_USER_ERROR = "账号或者密码错误!";
    /**
     * 验证码必须输入
     */
    public static final String VERIFY_CODE_NULL = "验证码必须输入!";
    /**
     * 验证码不正确
     */
    public static final String VERIFY_CODE_ERROR = "验证码不正确!";
    /**
     * 文件上传发生异常
     */
    public static final String FILE_UPLOAD_ERROR = "文件上传发生异常!";
    /**
     * Bean拷贝发生异常
     */
    public static final String BEAN_COPY_ERROR = "Bean拷贝发生异常!";

    /**
     * 入库总数量不能为空!
     */
    public static final String STORE_TOTAL_COUNT_NOT_EMPTY = "入库总数量不能为空!";
    /**
     * 保质期截止日期不能为空!
     */
    public static final String QUALITY_END_DATE_NOT_EMPTY = "保质期截止日期不能为空!";
    /**
     * 生产日期不能为空!
     */
    public static final String PRODUCTION_DATE_NOT_EMPTY = "生产日期不能为空!";
    /**
     * 保质期低于预警值不能为空!
     */
    public static final String BELOW_QUALITY_NOT_EMPTY = "保质期低于预警值不能为空!";
    /**
     * 库存低于预警值不能为空!
     */
    public static final String BELOW_STORE_NOT_EMPTY = "库存低于预警值不能为空!";
    /**
     * /**
     * 产品名不能为空
     */
    public static final String PRODUCTION_NAME_NOT_EMPTY = "产品名不能为空!";
    /**
     * 产品代码不能为空
     */
    public static final String PRODUCTION_CODE_NOT_EMPTY = "产品代码不能为空!";
    /**
     *备注超过50字
     */
    public static final String REMARK_OVER_LIMITED_LENGTH= "备注超过50字!";

}
