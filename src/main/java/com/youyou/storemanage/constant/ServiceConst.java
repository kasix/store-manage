package com.youyou.storemanage.constant;

/**
 * 业务代码枚举
 *
 * @Description: TODO
 */
public enum ServiceConst {
    /**
     * 当请求成功无异常返回,当这个码返回基本确定业务走通
     */
    OK(9999, "成功"),
    /**
     * 1000 不需要错误码的提示消息的公用码
     */
    FAIL(1000, "失败"),
    /**
     * 当服务器发生异常:例如空指针出现此错误码
     */
    ERROR(-9999, "未知异常");
    /**
     * code
     */
    private int code;
    /**
     * desc
     */
    private  String desc;

    ServiceConst() {
    }

    ServiceConst(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    /**
     * 返回常量值
     *
     * @param serviceEnum serviceEnum
     * @return constantValue
     */
    public static String values(ServiceConst serviceEnum) {
        return serviceEnum.desc;
    }

    /**
     * 返回code
     *
     * @param serviceEnum serviceEnum
     * @return constantValue
     */
    public static int code(ServiceConst serviceEnum) {
        return serviceEnum.code;
    }

    public int getCode() {
        return code;
    }

    public ServiceConst setCode(int code) {
        this.code = code;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public ServiceConst setDesc(String desc) {
        this.desc = desc;
        return this;
    }
}
