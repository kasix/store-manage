package com.youyou.storemanage.constant;

/**
 * 错误代码枚举 这里放置需要错误码的常量
 *
 * @Description: TODO
 */
public enum ErrorCodeConst {


    /**
     * PASSWORD_NULL
     */
    PASSWORD_NULL(1001, "密码不应该为空!"),
    /**
     * PASSWORD_OR_USER_ERROR
     */
    PASSWORD_OR_USER_ERROR(1002, "账号或者密码错误!"),
    /**
     * VERIFY_CODE_NULL
     */
    VERIFY_CODE_NULL(1003, "验证码必须输入!"),
    /**
     * VERIFY_CODE_ERROR
     */
    VERIFY_CODE_ERROR(1004, "验证码不正确!"),
    /**
     * STORE_TOTAL_COUNT_NOT_EMPTY
     */
    STORE_TOTAL_COUNT_NOT_EMPTY(1005, "入库总数量不能为空!"),
    /**
     * QUALITY_END_DATE_NOT_EMPTY
     */
    QUALITY_END_DATE_NOT_EMPTY(1006, "保质期截止日期不能为空!"),
    /**
     * PRODUCTION_DATE_NOT_EMPTY
     */
    PRODUCTION_DATE_NOT_EMPTY(1007, "生产日期不能为空!"),
    /**
     * BELOW_QUALITY_NOT_EMPTY
     */
    BELOW_QUALITY_NOT_EMPTY(1008, "保质期低于预警值不能为空!"),
    /**
     * BELOW_STORE_NOT_EMPTY
     */
    BELOW_STORE_NOT_EMPTY(1009, "库存低于预警值不能为空!"),

    /**
     * PRODUCTION_NAME_NOT_EMPTY
     */
    PRODUCTION_NAME_NOT_EMPTY(1010, "产品名不能为空!"),
    /**
     * PRODUCTION_CODE_NOT_EMPTY
     */
    PRODUCTION_CODE_NOT_EMPTY(1011, "产品代码不能为空!"),
    /**
     * REMARK_OVER_LIMITED_LENGTH
     */
    REMARK_OVER_LIMITED_LENGTH(1011, "备注超过50字!");

    /**
     * code
     */
    private int code;
    /**
     * msg
     */
    private String desc;

    ErrorCodeConst() {
    }

    ErrorCodeConst(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public int getCode() {
        return code;
    }


    public String getDesc() {
        return desc;
    }

}
