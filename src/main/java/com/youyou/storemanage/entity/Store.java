package com.youyou.storemanage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.youyou.storemanage.constant.ExceptionConst;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

/**
 * <p>
 * 库存表
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Data
@Accessors(chain = true)
public class Store implements Serializable, Comparable<Store> {

    private static final long serialVersionUID = 1L;

    /**
     * 库存id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 产品代码
     */
    private String code;

    /**
     * 产品名
     */
    private String name;

    /**
     * 实时数量
     */
    private Integer currentCount;

    /**
     * 入库总数量
     */
    private Integer storeTotalCount;


    /**
     * 库存低于预警值
     */
    private Integer belowStore;

    /**
     * 保质期低于预警值
     */
    private Integer belowQuality;

    /**
     * 生产日期
     */
    private LocalDateTime productionDate;

    /**
     * 保质期截止日期
     */
    private LocalDateTime qualityEndDate;

    /**
     * 备注
     */
    private String remark;

    @Override
    public int compareTo(Store o) {
        // 取首字母拼音比较
        char at = this.getName().charAt(0);
        // 相同再按时间比较
        return at - o.getName().charAt(0) == 0 ? this.getQualityEndDate()
                                                     .compareTo(o.getQualityEndDate()) : at - o.getName().charAt(0);
    }
}

