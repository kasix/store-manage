package com.youyou.storemanage.entity;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 待出库暂存表
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("waiting_out_store")
public class WaitingOutStore implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 待出库暂存id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 产品代码
     */
    private String code;

    /**
     * 产品名
     */
    private String productionName;

    /**
     * 出库数量
     */
    private Integer storeOutCount;

    /**
     * 当时剩余数量
     */
    private Integer surplus;

    /**
     * 出库时间
     */
    private LocalDateTime outDate;

    /**
     * 操作人
     */
    private String operationPerson;

    /**
     * 消耗  1 未消耗 0 消耗
     */
    private Integer status;
    /**
     * 库存id
     */
    private Integer storeId;
    /**
     * 出库时保质期
     */
    private Integer qualityDay;

    public void setQualityDay(LocalDateTime e) {
        this.qualityDay = (int) DateUtil.between(Date.from(e.atZone(ZoneId.systemDefault())
                                                            .toInstant()), new Date(), DateUnit.DAY);
    }

    public void setQualityDay(Integer qualityDay) {
        this.qualityDay = qualityDay;

    }
}
