package com.youyou.storemanage.entity;

import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 入库记录表
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("into_store_history")
public class IntoStoreHistory implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 入库记录id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 产品代码
     */
    private String code;

    /**
     * 产品名
     */
    private String productionName;

    /**
     * 入库数量
     */
    private Integer storeIntoCount;
    /**
     * 库存id
     */
    private Integer storeId;
    /**
     * 入库时保质期
     */
    private Integer qualityDay;
    /**
     * 入库时间
     */
    private LocalDateTime intoTime;

    /**
     * 操作人
     */
    private String operationPerson;

    public void setQualityDay(LocalDateTime e) {
        this.qualityDay = (int) DateUtil.between(Date.from(e.atZone(ZoneId.systemDefault())
                                                     .toInstant()), new Date(), DateUnit.DAY);
    }

    public void setQualityDay(Integer qualityDay) {
        this.qualityDay = qualityDay;
    }
}
