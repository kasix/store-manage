package com.youyou.storemanage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.List;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("sys_menu")
public class SysMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 菜单名
     */
    private String name;

    /**
     * 父菜单id
     */
    private Integer parentId;

    /**
     * 菜单路径
     */
    private String path;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 菜单状态 1 正常 0 删除
     */
    private Integer status;

    /**
     * meta
     */
    @TableField(exist = false)
    private Meta meta;
    /**
     * 子菜单集合
     */
    @TableField(exist = false)
    private List<SysMenu> childs;

    /**
     * 元
     */
    @Data
    public class Meta {

        /**
         * 菜单名
         */
        private String menuName;

        /**
         * 图标
         */
        private String icon;
    }


}
