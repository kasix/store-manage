package com.youyou.storemanage.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色菜单表
 * </p>
 *
 * @author 作者
 * @since 2022-03-07
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("role_menu")
public class RoleMenu implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 菜单Id
     */
      private Integer menuId;

    /**
     * 角色id
     */
      private Integer roleId;


}
