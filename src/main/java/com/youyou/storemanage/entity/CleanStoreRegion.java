package com.youyou.storemanage.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 清库暂存表
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("clean_store_region")
public class CleanStoreRegion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 清库暂存id
     */
      @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 产品代码
     */
    private String code;

    /**
     * 产品名
     */
    private String productionName;

    /**
     * 清出量
     */
    private Integer storeOutCount;

    /**
     * 浪费率
     */
    private String wasteRate;

    /**
     * 清出时间
     */
    private LocalDateTime qualityDate;

    /**
     * 操作人
     */
    private String operationPerson;

    /**
     * 删除  1 未删除 0 删除
     */
    private Integer status;


}
