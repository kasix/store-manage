package com.youyou.storemanage.vo;

import lombok.Data;

/**
 * @description TODO
 * @CreateTime 2022/3/7 13:34
 */
@Data
public class UserRoleVo {
    /**
     * 拥有角色 1 没有拥有 0
     */
    private Integer possess;
    /**
     * 用户id
     */
    private Integer userId;
    /**
     * 用户名
     */
    private String name;
    /**
     * 角色id
     */
    private Integer roleId;
}
