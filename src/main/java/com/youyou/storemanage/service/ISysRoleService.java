package com.youyou.storemanage.service;

import com.youyou.storemanage.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
public interface ISysRoleService extends IService<SysRole> {

}
