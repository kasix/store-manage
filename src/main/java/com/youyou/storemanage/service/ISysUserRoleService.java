package com.youyou.storemanage.service;

import com.youyou.storemanage.entity.SysUserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

}
