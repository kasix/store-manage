package com.youyou.storemanage.service;

import com.youyou.storemanage.entity.SysRoleMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色菜单表 服务类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

}
