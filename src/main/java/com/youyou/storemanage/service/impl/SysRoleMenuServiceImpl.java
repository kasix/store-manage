package com.youyou.storemanage.service.impl;

import com.youyou.storemanage.entity.SysRoleMenu;
import com.youyou.storemanage.mapper.SysRoleMenuMapper;
import com.youyou.storemanage.service.ISysRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {

}
