package com.youyou.storemanage.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.bloomfilter.filter.FNVFilter;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.youyou.storemanage.entity.IntoStoreHistory;
import com.youyou.storemanage.entity.Store;
import com.youyou.storemanage.mapper.IntoStoreHistoryMapper;
import com.youyou.storemanage.mapper.StoreMapper;
import com.youyou.storemanage.service.IStoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.youyou.storemanage.util.UserUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 库存表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
@RequiredArgsConstructor
public class StoreServiceImpl extends ServiceImpl<StoreMapper, Store> implements IStoreService {
    /**
     * * 入库记录表 Mapper 接口
     */
    private final IntoStoreHistoryMapper intoStoreHistoryMapper;
    /**
     * * 入库记录表 Mapper 接口
     */
    private final StoreMapper storeMapper;

    @Override
    public boolean saveOrUpdate(Store entity) {
        if (entity.getId() != null) {
            // 原有修改
            return super.saveOrUpdate(entity);
        }
        // id 等于null 说明是新增操作
        // 查询条件构建
        LambdaQueryWrapper<Store> wrapper = new QueryWrapper<Store>().lambda();
        // 当code 生产日期 保质期结束期 一样则原有产品覆盖叠加，否则新增。
        wrapper.eq(Store::getCode, entity.getCode())
               .eq(Store::getProductionDate, entity.getProductionDate())
               .eq(Store::getQualityEndDate, entity.getQualityEndDate());
        // 是否产品一样
        Store store = super.getOne(wrapper.select());
        if (store == null) {// 新产品
            entity.setId(null);
            int storeId = storeMapper.insert(entity);
            saveHistory(entity, storeId);
            return true;
        }
        // 原产品叠加数量
        store.setCurrentCount(store.getCurrentCount() + entity.getStoreTotalCount())
             .setStoreTotalCount(store.getStoreTotalCount() + entity.getStoreTotalCount());
        saveHistory(entity, store.getId());
        return super.saveOrUpdate(store);
    }

    private void saveHistory(Store entity, int storeId) {
        // 入库记录
        IntoStoreHistory po = new IntoStoreHistory();
        po.setCode(entity.getCode());
        po.setStoreId(storeId);
        po.setIntoTime(LocalDateTimeUtil.now());
        po.setProductionName(entity.getName());
        po.setStoreIntoCount(entity.getStoreTotalCount());
        po.setOperationPerson(UserUtil.getUser().getName());
        po.setQualityDay(entity.getQualityEndDate());
        intoStoreHistoryMapper.insert(po);
    }
}
