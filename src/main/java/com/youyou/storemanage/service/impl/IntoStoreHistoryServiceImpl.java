package com.youyou.storemanage.service.impl;

import com.youyou.storemanage.entity.IntoStoreHistory;
import com.youyou.storemanage.mapper.IntoStoreHistoryMapper;
import com.youyou.storemanage.service.IIntoStoreHistoryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 入库记录表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
public class IntoStoreHistoryServiceImpl extends ServiceImpl<IntoStoreHistoryMapper, IntoStoreHistory> implements IIntoStoreHistoryService {

}
