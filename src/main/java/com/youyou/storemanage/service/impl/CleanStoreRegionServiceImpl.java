package com.youyou.storemanage.service.impl;

import com.youyou.storemanage.entity.CleanStoreRegion;
import com.youyou.storemanage.mapper.CleanStoreRegionMapper;
import com.youyou.storemanage.service.ICleanStoreRegionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 清库暂存表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
public class CleanStoreRegionServiceImpl extends ServiceImpl<CleanStoreRegionMapper, CleanStoreRegion> implements ICleanStoreRegionService {

}
