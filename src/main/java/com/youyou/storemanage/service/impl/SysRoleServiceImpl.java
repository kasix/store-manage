package com.youyou.storemanage.service.impl;

import com.youyou.storemanage.entity.SysRole;
import com.youyou.storemanage.mapper.SysRoleMapper;
import com.youyou.storemanage.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
