package com.youyou.storemanage.service.impl;

import com.youyou.storemanage.entity.RoleMenu;
import com.youyou.storemanage.mapper.RoleMenuMapper;
import com.youyou.storemanage.service.IRoleMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色菜单表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-07
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements IRoleMenuService {

}
