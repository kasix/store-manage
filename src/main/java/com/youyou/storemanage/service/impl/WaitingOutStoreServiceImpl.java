package com.youyou.storemanage.service.impl;

import com.youyou.storemanage.entity.WaitingOutStore;
import com.youyou.storemanage.mapper.WaitingOutStoreMapper;
import com.youyou.storemanage.service.IWaitingOutStoreService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 待出库暂存表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
public class WaitingOutStoreServiceImpl extends ServiceImpl<WaitingOutStoreMapper, WaitingOutStore> implements IWaitingOutStoreService {

}
