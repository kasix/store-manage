package com.youyou.storemanage.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.youyou.storemanage.common.R;
import com.youyou.storemanage.common.ServiceException;
import com.youyou.storemanage.constant.ErrorCodeConst;
import com.youyou.storemanage.entity.SysUser;
import com.youyou.storemanage.mapper.SysUserMapper;
import com.youyou.storemanage.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

}
