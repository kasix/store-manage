package com.youyou.storemanage.service.impl;

import com.youyou.storemanage.entity.SysMenu;
import com.youyou.storemanage.mapper.SysMenuMapper;
import com.youyou.storemanage.service.ISysMenuService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements ISysMenuService {

}
