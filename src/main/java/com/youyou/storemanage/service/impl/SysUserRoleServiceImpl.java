package com.youyou.storemanage.service.impl;

import com.youyou.storemanage.entity.SysUserRole;
import com.youyou.storemanage.mapper.SysUserRoleMapper;
import com.youyou.storemanage.service.ISysUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {

}
