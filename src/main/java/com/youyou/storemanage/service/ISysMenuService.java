package com.youyou.storemanage.service;

import com.youyou.storemanage.entity.SysMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
public interface ISysMenuService extends IService<SysMenu> {

}
