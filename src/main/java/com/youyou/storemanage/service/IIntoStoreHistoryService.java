package com.youyou.storemanage.service;

import com.youyou.storemanage.entity.IntoStoreHistory;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 入库记录表 服务类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
public interface IIntoStoreHistoryService extends IService<IntoStoreHistory> {

}
