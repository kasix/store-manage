package com.youyou.storemanage.service;

import com.youyou.storemanage.entity.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
public interface ISysUserService extends IService<SysUser> {

}
