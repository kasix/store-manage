package com.youyou.storemanage.service;

import com.youyou.storemanage.entity.WaitingOutStore;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 待出库暂存表 服务类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
public interface IWaitingOutStoreService extends IService<WaitingOutStore> {

}
