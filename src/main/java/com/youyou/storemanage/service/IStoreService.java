package com.youyou.storemanage.service;

import com.youyou.storemanage.entity.Store;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 库存表 服务类
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
public interface IStoreService extends IService<Store> {

}
