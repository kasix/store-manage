package com.youyou.storemanage.common;

import cn.dev33.satoken.exception.DisableLoginException;
import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotRoleException;
import cn.hutool.core.util.StrUtil;
import com.youyou.storemanage.constant.ErrorCodeConst;
import com.youyou.storemanage.constant.ServiceConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.Collections;

/**
 * 全局异常处理器
 *
 * @Description: 全局异常处理器
 * @Version 1.0
 */

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     * 校验异常处理
     *
     * @param ex ex
     * @return R
     */
    @ExceptionHandler({ConstraintViolationException.class})
    public R validExpectException(ConstraintViolationException ex) {
        log.error("校验异常处理===>", ex);
        return R.fail(Collections.EMPTY_LIST, ServiceConst.FAIL.getCode(), ex.getMessage());
    }

    /**
     * Jsr303异常处理
     *
     * @param ex ex
     * @return R
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    public R validExpectException(MethodArgumentNotValidException ex) {
        log.error("Jsr303异常处理===>", ex);
        return R.fail(Collections.EMPTY_LIST, ServiceConst.FAIL.getCode(),
                ex.getBindingResult().getAllErrors().get(0).getDefaultMessage()
        );
    }

    /**
     * 顶级异常处理
     *
     * @return R
     */
    @ExceptionHandler({Exception.class, RuntimeException.class})
    public R unExpectException(Exception ex) {
        log.error("顶级异常处理===>", ex);
        return new R(Collections.EMPTY_LIST,
                ServiceConst.ERROR.getCode(),
                ServiceConst.ERROR.getDesc());
    }

    /**
     * 业务异常处理
     *
     * @param ex ex
     * @return R
     */
    @ExceptionHandler({ServiceException.class})
    public R serviceException(ServiceException ex) {
        log.error("业务异常处理===>", ex);
        ErrorCodeConst errorCodeEnum = ex.getErrorCodeConst();
        if (errorCodeEnum == null) {
            return new R(Collections.EMPTY_LIST,
                    ServiceConst.FAIL.getCode(),
                    StrUtil.isBlank(ex.getMessage()) ? ex.getCause().getMessage() : ex.getMessage());
        } else {
            return new R(Collections.EMPTY_LIST,
                    errorCodeEnum.getCode(),
                    errorCodeEnum.getDesc());
        }

    }

    // 全局异常拦截（拦截项目中的NotLoginException异常）
    @ExceptionHandler(NotLoginException.class)
    public R handlerNotLoginException(NotLoginException nle, HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        // 打印堆栈，以供调试
        nle.printStackTrace();

        // 判断场景值，定制化异常信息
        String message = "";
        if (nle.getType().equals(NotLoginException.NOT_TOKEN)) {
            message = "未提供token";
        } else if (nle.getType().equals(NotLoginException.INVALID_TOKEN)) {
            message = "token无效";
        } else if (nle.getType().equals(NotLoginException.TOKEN_TIMEOUT)) {
            message = "token已过期";
        } else if (nle.getType().equals(NotLoginException.BE_REPLACED)) {
            message = "token已被顶下线";
        } else if (nle.getType().equals(NotLoginException.KICK_OUT)) {
            message = "token已被踢下线";
        } else {
            message = "当前会话未登录";
        }

        // 返回给前端
        return R.fail(message);
    }

    @ExceptionHandler(NotRoleException.class)
    public R handlerException(Exception e, HttpServletRequest request, HttpServletResponse response) {

        NotRoleException ee = (NotRoleException) e;
        // 返回给前端
        String msg = "";
        switch (ee.getRole()) {
            case "1":
                msg = "采购";
                break;
            case "2":
                msg = "打荷";
                break;
            case "3":
                msg = "后厨管理";
                break;
            case "4":
                msg = "管理员";
                break;
        }
        return R.fail("无此角色：" + msg);
    }

}