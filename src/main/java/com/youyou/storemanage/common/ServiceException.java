package com.youyou.storemanage.common;

import com.youyou.storemanage.constant.ErrorCodeConst;
import lombok.Data;

/**
 * 业务异常
 * @Description: 业务异常
 */
@Data
public class ServiceException extends RuntimeException {

    /**
     * 错误枚举
     */
    private ErrorCodeConst errorCodeConst;

    /**
     * 构造器
     */
    public ServiceException() {
        super();
    }


    /**
     * serviceExc
     *
     * @param message msg
     * @param cause   cause
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * serviceExc
     *
     * @param message msg
     */

    public ServiceException(String message) {
        super(message);
    }

    /**
     * serviceExc
     *
     * @param errorCodeConst errorCodeConst
     */

    public ServiceException(ErrorCodeConst errorCodeConst) {
        super(errorCodeConst.getDesc());
        this.errorCodeConst = errorCodeConst;

    }


    /**
     * throwable
     *
     * @param cause cas
     */
    public ServiceException(Throwable cause) {
        super(cause);
    }
}
