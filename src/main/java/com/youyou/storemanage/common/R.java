package com.youyou.storemanage.common;


import com.youyou.storemanage.constant.ErrorCodeConst;
import com.youyou.storemanage.constant.ServiceConst;
import lombok.Data;

/**
 * 通用返回体
 * @Description: TODO
 */
@Data
public class R<T> {
    /**
     * 响应码
     */
    private int code;
    /**
     * 响应信息
     */
    private String msg;
    /**
     * 响应数据
     */
    private T res;

    public R() {

    }

    public R(Builder<T> builder) {
        this.code = builder.code;
        this.msg = builder.msg;
        this.res = builder.data;

    }

    public R(T res, int code, String msg) {
        this.res = res;
        this.code = code;
        this.msg = msg;

    }

    public R(int code, String msg, T res) {
        this.code = code;
        this.msg = msg;
        this.res = res;
    }

    public R(int code) {
        this.code = code;
    }

    public R(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    /**
     * 构建者模式
     *
     * @param <T> T
     */
    @Data
    public static class Builder<T> {
        /**
         * 响应码
         */
        private int code;
        /**
         * 响应信息
         */
        private String msg;
        /**
         * 响应数据
         */
        private T data;

        public R<T> build() {
            return new R<T>(this);
        }

        public Builder<T> setCode(int code) {
            this.code = code;
            return this;
        }

        public Builder<T> setMsg(String msg) {
            this.msg = msg;
            return this;
        }

        public Builder<T> setData(T data) {
            this.data = data;
            return this;
        }

        public Builder<T> withData(T data) {
            this.data = data;
            this.code = ServiceConst.code(ServiceConst.OK);
            this.msg = ServiceConst.values(ServiceConst.OK);
            return this;
        }
    }


    /**
     * ok
     *
     * @return R
     */
    public static <T> R<T> ok() {
        return new R<>(ServiceConst.code(ServiceConst.OK), ServiceConst.values(ServiceConst.OK));
    }

    /**
     * ok
     *
     * @return R
     */
    public static <T> R<T> ok(T data) {
        return new R<>(data, ServiceConst.code(ServiceConst.OK), ServiceConst.values(ServiceConst.OK));
    }

    /**
     * ok
     *
     * @return R
     */
    public static <T> R<T> ok(ErrorCodeConst ErrorCodeConst) {
        return new R<>(null, ErrorCodeConst.getCode(), ErrorCodeConst.getDesc());
    }

    /**
     * fail
     *
     * @return R
     */
    public static <T> R<T> fail() {
        return new R<>(ServiceConst.code(ServiceConst.FAIL), ServiceConst.values(ServiceConst.FAIL));
    }

    /**
     * fail
     *
     * @param ErrorCodeConst 错误枚举
     * @return R
     */
    public static <T> R<T> fail(ErrorCodeConst ErrorCodeConst) {
        return new R<>(ErrorCodeConst.getCode(), ErrorCodeConst.getDesc());
    }

    /**
     * fail
     * @param msg 消息
     * @return R
     */
    public static <T> R<T> fail(String msg) {
        return new R<>(ServiceConst.FAIL.getCode(), msg);
    }

    /**
     * fail
     *
     * @return R
     */
    public static <T> R<T> fail(T data, int code, String msg) {
        return new R<>(data, code, msg);
    }
}
