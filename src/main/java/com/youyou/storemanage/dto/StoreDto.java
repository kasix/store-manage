package com.youyou.storemanage.dto;

import com.youyou.storemanage.constant.ExceptionConst;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 库存表
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Data
public class StoreDto implements Serializable {


    /**
     * 产品代码
     */
    @NotBlank(message = ExceptionConst.PRODUCTION_CODE_NOT_EMPTY)
    private String code;

    /**
     * 产品名
     */
    @NotBlank(message = ExceptionConst.PRODUCTION_NAME_NOT_EMPTY)
    private String name;


    /**
     * 入库总数量
     */
    @NotNull(message = ExceptionConst.STORE_TOTAL_COUNT_NOT_EMPTY)
    private Integer storeTotalCount;

    /**
     * 实时保质期剩余天数
     */
    private Integer currentQualityDay;

    /**
     * 库存低于预警值
     */
    @NotNull(message = ExceptionConst.BELOW_STORE_NOT_EMPTY)
    private Integer belowStore;

    /**
     * 保质期低于预警值
     */
    @NotNull(message = ExceptionConst.BELOW_QUALITY_NOT_EMPTY)
    private Integer belowQuality;

    /**
     * 生产日期
     */
    @NotNull(message = ExceptionConst.PRODUCTION_DATE_NOT_EMPTY)
    private LocalDateTime productionDate;


    /**
     * 保质期截止日期
     */
    @NotNull(message = ExceptionConst.QUALITY_END_DATE_NOT_EMPTY)
    private LocalDateTime qualityEndDate;

    /**
     * 备注
     */
    @Length(max = 50,message = ExceptionConst.REMARK_OVER_LIMITED_LENGTH)
    private String remark;


}
