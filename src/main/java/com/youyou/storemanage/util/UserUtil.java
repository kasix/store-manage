package com.youyou.storemanage.util;

import cn.dev33.satoken.stp.StpUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.google.gson.Gson;
import com.sun.org.apache.bcel.internal.generic.RET;
import com.youyou.storemanage.entity.SysUser;
import com.youyou.storemanage.entity.SysUserRole;
import com.youyou.storemanage.mapper.SysUserMapper;
import com.youyou.storemanage.mapper.SysUserRoleMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 用户工具类
 *
 * @description 用户工具类
 */
@Component
@Slf4j
public class UserUtil {

    /**
     * user
     */
    private static SysUserMapper userMapper;
    /**
     * userRoleMapper
     */
    private static SysUserRoleMapper userRoleMapper;
    /**
     * gson
     */
    private static Gson gson;


    @Autowired
    public void setGson(Gson gson) {
        UserUtil.gson = gson;
    }

    @Autowired
    public void setUserMapper(SysUserMapper userMapper) {
        UserUtil.userMapper = userMapper;
    }

    @Autowired
    public void setUserRoleMapper(SysUserRoleMapper userRoleMapper) {
        UserUtil.userRoleMapper = userRoleMapper;
    }

    /**
     * 获取用户对象
     *
     * @return 用户对象
     */
    public static SysUser getUser() {
        Integer userId = Integer.valueOf(StpUtil.getLoginId().toString());
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>(SysUser.class)
                .eq(SysUser::getId, userId);
        return userMapper.selectOne(wrapper);
    }

    /**
     * 获取角色id
     *
     * @return roleId
     */
    public static List<Integer> getRoleIds() {
        Integer userId = Integer.valueOf(StpUtil.getLoginId().toString());
        LambdaQueryWrapper<SysUserRole> wrapper = new LambdaQueryWrapper<>(SysUserRole.class)
                .eq(SysUserRole::getUserId, userId)
                .select(e -> e.getProperty().equals("roleId"));
        return userRoleMapper.selectList(wrapper).stream().map(SysUserRole::getRoleId).collect(Collectors.toList());

    }
}
