package com.youyou.storemanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动
 */
@SpringBootApplication
public class StoreManageApplication {

    public static void main(String[] args) {
        SpringApplication.run(StoreManageApplication.class, args);
    }

}
