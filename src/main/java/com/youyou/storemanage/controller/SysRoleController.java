package com.youyou.storemanage.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youyou.storemanage.common.R;
import com.youyou.storemanage.entity.SysRole;
import com.youyou.storemanage.entity.SysUser;
import com.youyou.storemanage.entity.SysUserRole;
import com.youyou.storemanage.mapper.SysUserMapper;
import com.youyou.storemanage.mapper.SysUserRoleMapper;
import com.youyou.storemanage.service.ISysUserRoleService;
import com.youyou.storemanage.vo.UserRoleVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/sysRole")
public class SysRoleController extends BaseController {

    /**
     * 用户角色表 服务类
     */
    private final ISysUserRoleService userRoleService;
    /**
     * 用户角色表 Mapper 接口
     */
    private final SysUserRoleMapper userRoleMapper;
    /**
     * 用户表 Mapper 接口
     */
    private final SysUserMapper userMapper;

    /**
     * 分页查询角色
     *
     * @return SearchResult
     * @apiNote <p>
     * https://searcher.ejlchina.com/guide/latest/start.html#开始检索   参考搜索条件的组装
     * </p>
     * @see {@link "https://searcher.ejlchina.com/guide/latest/start.html#开始检索"}   参考搜索条件的组装
     */
    @GetMapping
    @Override
    public R<Page<SysRole>> beanSearchPage() {
        return super.page(SysRole.class);

    }

    /**
     * 分配角色
     *
     * @param userIds 用户id
     * @param roleId  角色id
     * @return R
     */
    @PostMapping("/link")
    public R distributeRole(@RequestParam("userIds") List<Integer> userIds, Integer roleId) {
        userRoleMapper.deleteLink(roleId);
        List<SysUserRole> roles = userIds.stream().map(e -> {
            SysUserRole userRole = new SysUserRole();
            userRole.setRoleId(roleId);
            userRole.setUserId(e);
            return userRole;
        }).collect(Collectors.toList());
        userRoleService.saveBatch(roles);
        return R.ok();
    }

    /**
     * 查询角色关联和未关联用户
     *
     * @param roleId 角色id
     * @return R
     */
    @GetMapping("/link")
    public R<List<UserRoleVo>> queryLink(Integer roleId) {
        return R.ok(userRoleMapper.queryUnlinkAndLink(roleId).stream().map(
                e -> {
                    e.setRoleId(roleId);
                    SysUser user = userMapper.selectById(e.getUserId());
                    e.setName(user.getName());
                    return e;
                }
        ).collect(Collectors.toList()));
    }

}
