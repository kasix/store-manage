package com.youyou.storemanage.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youyou.storemanage.common.R;
import com.youyou.storemanage.entity.Store;
import com.youyou.storemanage.entity.WaitingOutStore;
import com.youyou.storemanage.mapper.WaitingOutStoreMapper;
import com.youyou.storemanage.service.IStoreService;
import com.youyou.storemanage.service.IWaitingOutStoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 待出库暂存表 前端控制器
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/waitingOutStore")
public class WaitingOutStoreController extends BaseController {
    /**
     * 库存表 服务类
     */
    private final IStoreService iStoreService;
    /**
     * 待出库暂存表 服务类
     */
    private final IWaitingOutStoreService waitingOutStoreService;
    /**
     * 待出库暂存表 Mapper 接口
     */
    private final WaitingOutStoreMapper waitingOutStoreMapper;

    /**
     * 分页查询出库记录
     *
     * @return SearchResult
     * @apiNote <p>
     * https://searcher.ejlchina.com/guide/latest/start.html#开始检索   参考搜索条件的组装
     * </p>
     * @see {@link "https://searcher.ejlchina.com/guide/latest/start.html#开始检索"}   参考搜索条件的组装
     */
    @GetMapping("/out")
    @Override
    public R<Page<WaitingOutStore>> beanSearchPage() {
        return super.page(WaitingOutStore.class);

    }

    /**
     * 产品回退
     *
     * @param id 待出库区id
     * @return R
     */
    @DeleteMapping("/withdraw")
    public R withdraw(Integer id) {
        WaitingOutStore outStore = waitingOutStoreService.getById(id);
        // 获取原数据
        Store store = iStoreService.getById(outStore.getStoreId());
        // 恢复原数量
        store.setCurrentCount(store.getCurrentCount() + outStore.getStoreOutCount());
        iStoreService.saveOrUpdate(store);
        // 回退
        waitingOutStoreMapper.delete(id);
        return R.ok();
    }

    /**
     * 产品已消耗
     *
     * @param id 待出库区id
     * @return R
     */
    @DeleteMapping("/cost")
    public R cost(Integer id) {
        // 已消耗 && 生成记录
        waitingOutStoreService.removeById(id);
        return R.ok();
    }
}
