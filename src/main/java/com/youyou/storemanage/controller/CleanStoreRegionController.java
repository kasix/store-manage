package com.youyou.storemanage.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youyou.storemanage.common.R;
import com.youyou.storemanage.entity.CleanStoreRegion;
import com.youyou.storemanage.entity.WaitingOutStore;
import com.youyou.storemanage.mapper.CleanStoreRegionMapper;
import com.youyou.storemanage.service.ICleanStoreRegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 清库暂存表 前端控制器
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */

@RequiredArgsConstructor
@RestController
@RequestMapping("/cleanStoreRegion")
public class CleanStoreRegionController extends BaseController {
    /**
     * 清库暂存表 Mapper 接口
     */
    private final ICleanStoreRegionService cleanStoreRegionService;

    /**
     * 分页查询出库记录
     *
     * @return SearchResult
     * @apiNote <p>
     * https://searcher.ejlchina.com/guide/latest/start.html#开始检索   参考搜索条件的组装
     * </p>
     * @see {@link "https://searcher.ejlchina.com/guide/latest/start.html#开始检索"}   参考搜索条件的组装
     */
    @GetMapping
    @Override
    public R<Page<CleanStoreRegion>> beanSearchPage() {
        return super.page(CleanStoreRegion.class);
    }

    /**
     * 产品删除
     *
     * @param id 清出区id
     * @return R
     */
    @DeleteMapping
    public R delete(Integer id) {
        // 删除 && 生成记录
        cleanStoreRegionService.removeById(id);
        return R.ok();
    }
}
