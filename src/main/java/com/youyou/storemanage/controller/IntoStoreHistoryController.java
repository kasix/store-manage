package com.youyou.storemanage.controller;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youyou.storemanage.common.R;
import com.youyou.storemanage.entity.IntoStoreHistory;
import com.youyou.storemanage.entity.Store;
import com.youyou.storemanage.vo.StoreVo;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 入库记录表 前端控制器
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@RestController
@RequestMapping("/intoStoreHistory")
public class IntoStoreHistoryController extends BaseController {
    /**
     * 分页查询入库记录
     *
     * @return SearchResult
     * @apiNote <p>
     * https://searcher.ejlchina.com/guide/latest/start.html#开始检索   参考搜索条件的组装
     * </p>
     * @see {@link "https://searcher.ejlchina.com/guide/latest/start.html#开始检索"}   参考搜索条件的组装
     */
    @GetMapping
    @Override
    public R<Page<IntoStoreHistory>> beanSearchPage() {
        return super.page(IntoStoreHistory.class);

    }


}
