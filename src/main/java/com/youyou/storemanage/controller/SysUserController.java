package com.youyou.storemanage.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.youyou.storemanage.common.R;
import com.youyou.storemanage.common.ServiceException;
import com.youyou.storemanage.constant.ErrorCodeConst;
import com.youyou.storemanage.entity.*;
import com.youyou.storemanage.mapper.SysUserMapper;
import com.youyou.storemanage.service.IRoleMenuService;
import com.youyou.storemanage.service.ISysMenuService;
import com.youyou.storemanage.service.ISysUserRoleService;
import com.youyou.storemanage.service.ISysUserService;
import com.youyou.storemanage.util.UserUtil;
import com.youyou.storemanage.vo.StoreVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@RestController
@RequestMapping("/sysUser")
@RequiredArgsConstructor
public class SysUserController extends BaseController {
    /**
     * 系统用户
     */
    private final ISysUserService sysUserService;

    /**
     * 菜单表 服务类
     */
    private final ISysMenuService menuService;
    /**
     * 角色菜单表 服务类
     */
    private final IRoleMenuService roleMenuService;
    /**
     * 用户角色表 服务类
     */
    private final ISysUserRoleService roleService;
    /**
     * 系统用户登录
     *
     * @param user 系统用户请求体
     * @return 系统用户响应
     */

    @PostMapping("/login")
    public R<Map<String, String>> login(@RequestBody SysUser user) {

        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysUser::getName, user.getName())
               .eq(SysUser::getPassword, user.getPassword());

        SysUser one = sysUserService.getOne(wrapper);
        if (one == null)
            throw new ServiceException(ErrorCodeConst.PASSWORD_OR_USER_ERROR);
        // 标记登录
        StpUtil.login(one.getId());
        HashMap<String, String> map = new HashMap<>();
        map.put("token", StpUtil.getTokenValue());
        return R.ok(map);
    }

    /**
     * 新增系统用户
     *
     * @param user 系统用户请求体
     * @return R
     */

    @PostMapping("/add")
    public R addUser(@RequestBody SysUser user) {
        sysUserService.save(user);
        SysUserRole entity = new SysUserRole();
        entity.setUserId(user.getId());
        entity.setRoleId(1);//默认打荷角色
        roleService.save(entity);
        return R.ok();
    }

    /**
     * 修改系统用户
     *
     * @param user 系统用户请求体
     * @return R
     */

    @PutMapping
    public R updateUser(@RequestBody SysUser user) {
        sysUserService.saveOrUpdate(user);
        return R.ok();
    }


    /**
     * 分页查询用户
     *
     * @return SearchResult
     * @apiNote <p>
     * https://searcher.ejlchina.com/guide/latest/start.html#开始检索   参考搜索条件的组装
     * </p>
     * @see {@link "https://searcher.ejlchina.com/guide/latest/start.html#开始检索"}   参考搜索条件的组装
     */
    @GetMapping
    @Override
    public R<Page<SysUser>> beanSearchPage() {
        return super.page(SysUser.class);
    }

    /**
     * 查询系统菜单
     *
     * @return SysMenu
     */
    @GetMapping("/menu")
    public R<List<SysMenu>> all() {

        List<SysMenu> menus = menuService.list();
        List<SysMenu> root = menus.stream().filter(e -> e.getParentId() == null).collect(Collectors.toList());
        List<SysMenu> childs = menus.stream().filter(e -> e.getParentId() != null).collect(Collectors.toList());
        for (SysMenu sysMenu : root) {
            // 创建子节点集合
            sysMenu.setChilds(new ArrayList<>());
            addChild(childs, sysMenu);
        }

        // 筛选菜单
        LambdaQueryWrapper<RoleMenu> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(RoleMenu::getRoleId, UserUtil.getRoleIds());

        // 查询角色拥有的菜单
        List<Integer> menuIds = roleMenuService.list(wrapper)
                                               .stream()
                                               .map(RoleMenu::getMenuId)
                                               .collect(Collectors.toList());
        List<SysMenu> possessMenu = root.stream().filter(e -> menuIds.contains(e.getId())).collect(Collectors.toList());
        return R.ok(possessMenu);
    }


    /**
     * 子节点添加
     *
     * @param childs 子节点集合
     * @param parent 父节点
     */
    private void addChild(List<SysMenu> childs, SysMenu parent) {

        for (SysMenu child : childs) {
            if (child.getParentId().equals(parent.getId())) {
                // 是对应子节点
                parent.getChilds().add(child);
                // 寻找该节点的子节点
                child.setChilds(new ArrayList<>());
                addChild(childs, child);
            }

        }

    }

}
