package com.youyou.storemanage.controller;

import cn.hutool.core.util.PageUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ejlchina.searcher.BeanSearcher;
import com.ejlchina.searcher.SearchResult;
import com.ejlchina.searcher.boot.BeanSearcherProperties;
import com.ejlchina.searcher.util.MapUtils;
import com.youyou.storemanage.common.R;
import com.youyou.storemanage.entity.Store;
import com.youyou.storemanage.util.RequestContextUtil;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * 接口基类
 *
 * @description TODO
 */
public abstract class BaseController {
    /**
     * 注入 Bean 检索器，它检索出来的数据以 泛型 对象呈现
     */
    @Autowired
    protected BeanSearcher beanSearcher;
    /**
     * beanSearcherProperties
     */
    @Autowired
    protected BeanSearcherProperties beanSearcherProperties;

    /**
     * 分页查询
     *
     * @return R
     * @apiNote <p>
     * https://searcher.ejlchina.com/guide/latest/start.html#开始检索   参考搜索条件的组装
     * </p>
     * @see {@link "https://searcher.ejlchina.com/guide/latest/start.html#开始检索"}   参考搜索条件的组装
     */
    public final <T> R<Page<T>> page(Class<T> clazz) {
        HttpServletRequest request = RequestContextUtil.getRequest();
        SearchResult<T> result = beanSearcher.search(clazz, MapUtils.flat(request.getParameterMap()));

        int currentPage = StrUtil.isBlank(request.getParameter("page")) ? beanSearcherProperties.getParams()
                                                                                                .getPagination()
                                                                                                .getStart() : Integer.parseInt(request
                .getParameter("page"));
        int size = StrUtil.isBlank(request.getParameter("size")) ? beanSearcherProperties.getParams()
                                                                                         .getPagination()
                                                                                         .getDefaultSize() : Integer
                .parseInt(request.getParameter("size"));
        int totalPage = PageUtil.totalPage(result.getTotalCount().intValue(), size);

        Page<T> page = new Page<T>(currentPage, size, totalPage);
        page.setTotal(result.getTotalCount().intValue());
        page.setRecords(result.getDataList());
        hook(page);
        return R.ok(page);


    }

    /**
     * 模板方法
     *
     * @return R
     */
    public <T> R<T> beanSearchPage() {
        return R.ok();
    }

    public  void hook(Page page) {

    }
}
