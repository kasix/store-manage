package com.youyou.storemanage.controller;

import cn.dev33.satoken.annotation.SaCheckRole;
import cn.dev33.satoken.annotation.SaMode;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ejlchina.searcher.SearchResult;
import com.youyou.storemanage.common.R;
import com.youyou.storemanage.dto.StoreDto;
import com.youyou.storemanage.entity.CleanStoreRegion;
import com.youyou.storemanage.entity.Store;
import com.youyou.storemanage.entity.WaitingOutStore;
import com.youyou.storemanage.mapper.StoreMapper;
import com.youyou.storemanage.service.ICleanStoreRegionService;
import com.youyou.storemanage.service.IStoreService;
import com.youyou.storemanage.service.IWaitingOutStoreService;
import com.youyou.storemanage.util.RequestContextUtil;
import com.youyou.storemanage.util.UserUtil;
import com.youyou.storemanage.vo.StoreVo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 库存表 前端控制器
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@RequiredArgsConstructor // 构造注入
@RestController
@RequestMapping("/store")
@Slf4j//日志
public class StoreController extends BaseController {


    /**
     * 库存表 服务类
     */
    private final IStoreService iStoreService;
    /**
     * 待出库暂存表 服务类
     */
    private final IWaitingOutStoreService waitingOutStoreService;
    /**
     * 清库暂存表 服务类
     */
    private final ICleanStoreRegionService cleanStoreRegionService;
    /**
     * 库存表 Mapper 接口
     */
    private final StoreMapper storeMapper;

    /**
     * 产品入库
     * 1	打荷
     * 2	采购
     * 3	后厨管理
     * 4	管理员
     *
     * @param storeDto storeDto
     * @return R
     */
    @SaCheckRole(value = {"2", "3", "4"}, mode = SaMode.OR) // 1->打荷角色id
    @PostMapping("/into")
    public R intoStore(@RequestBody @Valid StoreDto storeDto) {
        // bean拷贝
        Store entity = BeanUtil.copyProperties(storeDto, Store.class);
        entity.setCurrentCount(storeDto.getStoreTotalCount());
        iStoreService.saveOrUpdate(entity);
        return R.ok();
    }

    /**
     * 产品详情
     *
     * @param storeId 库存Id
     * @return R
     */

    @GetMapping("/detail/{storeId}")
    public R<Store> detail(@PathVariable Integer storeId) {
        return R.ok(iStoreService.getById(storeId));
    }

    /**
     * 产品修改
     *
     * @param id       库存id
     * @param storeDto storeDto
     * @return R
     */
    @PutMapping("/{id}")
    public R updateStore(@PathVariable Integer id, @RequestBody @Validated StoreDto storeDto) {
        // bean拷贝
        Store entity = BeanUtil.copyProperties(storeDto, Store.class);
        entity.setId(id);
        iStoreService.saveOrUpdate(entity);
        return R.ok();
    }

    /**
     * 产品清出
     *
     * @param id 库存id
     * @return R
     */
    @DeleteMapping("/cleanStore")
    public R cleanStore(Integer id) {
        // 获取原数据
        Store store = iStoreService.getById(id);
        CleanStoreRegion entity = new CleanStoreRegion();
        // 清出量
        BigDecimal decimal = new BigDecimal(store.getCurrentCount());
        // 总库存量
        BigDecimal total = new BigDecimal(store.getStoreTotalCount());
        // 浪费率
        BigDecimal wasteRate = decimal.divide(total, 3, RoundingMode.HALF_UP);
        String format = NumberUtil.decimalFormat("###.###%", wasteRate);
        entity.setWasteRate(format);
        entity.setQualityDate(LocalDateTimeUtil.now());
        entity.setOperationPerson(UserUtil.getUser().getName());
        entity.setStatus(1);
        entity.setCode(store.getCode());
        // 构建清出信息
        entity.setProductionName(store.getName());
        entity.setStoreOutCount(store.getStoreTotalCount());
        cleanStoreRegionService.save(entity);
        // 物理删除
        storeMapper.delete(id);
        return R.ok();
    }


    /**
     * 产品出库
     * 1	打荷
     * 2	采购
     * 3	后厨管理
     * 4	管理员
     *
     * @param id       库存id
     * @param outCount 出库量
     * @return R
     */
    @PutMapping("/outStore")
    public R outStore(Integer id, Integer outCount) {
        // 获取原数据
        Store store = iStoreService.getById(id);
        WaitingOutStore entity = new WaitingOutStore();
        BeanUtil.copyProperties(store, entity, "id");
        // 构建出库信息
        entity.setProductionName(store.getName());
        entity.setStoreOutCount(outCount);
        entity.setOutDate(LocalDateTimeUtil.now());
        entity.setSurplus(store.getCurrentCount());
        entity.setStatus(1);
        entity.setStoreId(store.getId());
        entity.setOperationPerson(UserUtil.getUser().getName());
        entity.setQualityDay(store.getQualityEndDate());
        waitingOutStoreService.save(entity);
        // 减去原数量
        store.setCurrentCount(store.getCurrentCount() - outCount);
        iStoreService.saveOrUpdate(store);
        return R.ok();
    }

    @Override
    public void hook(Page page) {
        if (StrUtil.isBlank(RequestContextUtil.getRequest().getParameter("type"))) return;
        LambdaQueryWrapper<Store> queryWrapper = Wrappers.lambdaQuery();
        // 1 表示查询库存低位数据
        if ("1".equals(RequestContextUtil.getRequest().getParameter("type")))
            queryWrapper.ltSql(Store::getCurrentCount, "below_store");

        Page<Store> storePage = new Page<>(page.getCurrent(), page.getSize());
        IPage<Store> res = storeMapper.selectPage(storePage, queryWrapper);
        // 2 表示查询保质期低位数据
        if ("2".equals(RequestContextUtil.getRequest().getParameter("type"))) {
            res.setRecords(res.getRecords().stream().filter(e -> {

                int cu = (int) DateUtil.between(Date.from(e.getQualityEndDate()
                                                           .atZone(ZoneId.systemDefault())
                                                           .toInstant()), new Date(), DateUnit.DAY);
                return cu < e.getBelowQuality();
            }).collect(Collectors.toList()));


        }
        BeanUtil.copyProperties(res, page);

    }

    /**
     * 分页查询产品
     *
     * @return SearchResult
     * @apiNote <p>
     * https://searcher.ejlchina.com/guide/latest/start.html#开始检索   参考搜索条件的组装
     * </p>
     * @see {@link "https://searcher.ejlchina.com/guide/latest/start.html#开始检索"}   参考搜索条件的组装
     */
    @GetMapping
    @Override
    public R<Page<StoreVo>> beanSearchPage() {
        R<Page<Store>> page = super.page(Store.class);

        R<Page<StoreVo>> p = new R<Page<StoreVo>>();
        // 转换VO，设置剩余保质期天数
        List<StoreVo> storeVos = page.getRes().getRecords().stream().map(e -> {

            StoreVo storeVo = new StoreVo();
            BeanUtil.copyProperties(e, storeVo);
            storeVo.setQualityDay((int) DateUtil.between(Date.from(e.getQualityEndDate()
                                                                    .atZone(ZoneId.systemDefault())
                                                                    .toInstant()), new Date(), DateUnit.DAY));
            // 都低于优先库存
            if (storeVo.getQualityDay() < storeVo.getBelowQuality() && storeVo.getCurrentCount() < storeVo.getBelowStore()) {
                // 库存低于预警值
                storeVo.setLighting("EC808D");
                return storeVo;
            }
            if (storeVo.getQualityDay() < storeVo.getBelowQuality()) {
                // 低于保质期预警值
                storeVo.setLighting("FACD91");
                return storeVo;
            }
            if (storeVo.getCurrentCount() < storeVo.getBelowStore()) {
                // 库存低于预警值
                storeVo.setLighting("EC808D");

            }
            return storeVo;
        }).filter(e -> {
            // 保质期为0的清出
            if (e.getQualityDay() != 0)
                return true;
            // 删除
            cleanStore(e.getId());
            return false;

        }).filter(e -> {
            // 实时数量为0的清出
            if (e.getCurrentCount() != 0)
                return true;
            // 删除
            cleanStore(e.getId());
            return false;

        }).collect(Collectors.toList());
        BeanUtil.copyProperties(page, p);
        // 排序
        Collections.sort(p.getRes().getRecords());
        p.setRes(p.getRes().setRecords(storeVos));
        return p;
    }


}
