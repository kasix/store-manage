package com.youyou.storemanage.mapper;

import com.youyou.storemanage.entity.SysUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {

}
