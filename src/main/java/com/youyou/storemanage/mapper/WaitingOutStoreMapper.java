package com.youyou.storemanage.mapper;

import com.youyou.storemanage.entity.WaitingOutStore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 待出库暂存表 Mapper 接口
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Mapper
public interface WaitingOutStoreMapper extends BaseMapper<WaitingOutStore> {
    /**
     * 物理删除
     *
     * @param id 待出库id
     */

    @Delete("delete from waiting_out_store where id=#{id}")
    public void delete(Integer id);
}
