package com.youyou.storemanage.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.youyou.storemanage.entity.SysUserRole;
import com.youyou.storemanage.vo.UserRoleVo;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {
    /**
     * 查询为分配和已分配
     *
     * @param roleId 角色id
     * @return List
     */
    public List<UserRoleVo> queryUnlinkAndLink(@Param("roleId") Integer roleId);


    public void deleteLink( @Param("roleId") Integer roleId);
}
