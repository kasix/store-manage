package com.youyou.storemanage.mapper;

import com.youyou.storemanage.entity.Store;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 库存表 Mapper 接口
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Mapper
public interface StoreMapper extends BaseMapper<Store> {
    /**
     * 物理删除
     * @param id 库存id
     */
    @Delete("delete from store where id=#{id}")
    public void delete(Integer id);
}
