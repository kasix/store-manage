package com.youyou.storemanage.mapper;

import com.youyou.storemanage.entity.SysMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Mapper
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
