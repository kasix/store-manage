package com.youyou.storemanage.mapper;

import com.youyou.storemanage.entity.IntoStoreHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 入库记录表 Mapper 接口
 * </p>
 *
 * @author 作者
 * @since 2022-03-06
 */
@Mapper
public interface IntoStoreHistoryMapper extends BaseMapper<IntoStoreHistory> {



}
