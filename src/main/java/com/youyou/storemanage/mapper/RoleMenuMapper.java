package com.youyou.storemanage.mapper;

import com.youyou.storemanage.entity.RoleMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 角色菜单表 Mapper 接口
 * </p>
 *
 * @author 作者
 * @since 2022-03-07
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {

}
