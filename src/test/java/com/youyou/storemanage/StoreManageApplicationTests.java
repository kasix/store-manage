package com.youyou.storemanage;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Collections;

@RunWith(JUnit4.class)
public class StoreManageApplicationTests {

    /**
     *  配置参考
     * @see {@link "https://baomidou.com/pages/981406/ "}
     */
    @Test
    public void contextLoads() {

        FastAutoGenerator.create("jdbc:mysql://rm-bp1xcpue62m04u7a93o.mysql.rds.aliyuncs.com:7963/youyou?useUnicode=true&characterEncoding=UTF-8&serverTimezone=Asia/Shanghai", "kasix38411_11", "Kasix2311kkx")
                         .globalConfig(builder -> {
                             builder
                                     .fileOverride() // 覆盖已生成文件
                                     .outputDir("K:\\store-manage\\src\\main\\java"); // 指定输出目录
                         })
                         .packageConfig(builder -> {
                             builder.parent("com.youyou.storemanage")
                                    .pathInfo(Collections.singletonMap(OutputFile.xml, "K:\\store-manage\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                         }).strategyConfig(builder -> {

            builder.addInclude("role_menu")
                   .entityBuilder().enableLombok()
                   .enableChainModel()

                   .mapperBuilder().superClass(BaseMapper.class)
                   .enableMapperAnnotation()
                   .controllerBuilder().enableRestStyle();

        }).execute();


    }

}
